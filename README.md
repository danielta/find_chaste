Author: D. Tartarini (d.tartarini@sheffield.ac.uk)

Copyright 2014

License: BSD 3-clauses
==============================================================================
FindCHASTE.cmake can be used from a CMake compilation framework
to find CHASTE libraries and includes.

It allows to compile your code usin CHASTE libraries using CMake building system
CHASTE is a product of the University of Oxford: 
http://chaste.cs.ox.c.uk 

------------------------------------------------------------------------------
Installation instructions:

1) Compile and install Chaste library following instructions on:

https://chaste.cs.ox.ac.uk/trac/wiki/InstallGuides/InstallGuide

2) Set the environment variables to the folders contatinig Chaste and PETSc 
   libraries and includes.


  export PETSC_DIR=/path/to/PETSc
  export CHASTE_DIR=/path/to/CHASTE

3) cmake .

4) make

5) run ./test_find_chaste

you will see Chaste running and producing a profile file in the same folder
 and the optput in /tmp/<user>/chaste_output


For Bugs or improvent do not hesitaty to contact the author.
