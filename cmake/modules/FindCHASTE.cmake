################################################################################
# FindCHASTE.cmake
# This code will find Chaste library and will define the following variables:
#
# CHASTE_FOUND         library has been found on the system
# CHASTE_INCLUDE_DIRS  Chaste include directories
# CHASTE_LIBRARY_DIRS  Chaste library directories
# CHASTE_LIBRARIES     Chaste Libraries
# CHASTE_DEP_LIBS      Libraries required by Chaste
# CHASTE_DEP_LIBS_DIRS Libraries required by Chaste
# CHASTE_DEP_INCLUDES  Include Dir required by Chaste
# this CMake script look in system dir for Chaste library or in the
# directories defined in CHASTE_DIR environment variable
#
# Dependencies:
# Boost, HDF5, UMFPack, Openmpi, PETSc, Lapack, Metis, Parmetis, VTK 
#
# TODO: add find for specific version of Chaste the supported library version
# combination
# TODO add contol on version and improve error message 
################################################################################

#  # Look for Chaste
  find_path(CHASTE_INCLUDE_DIRS
          Version.hpp
          HINTS "$ENV{CHASTE_DIR}/include" "${CHASTE_DIR}/includes"
          )
   
  find_path(CHASTE_LIBRARY_DIRS
          libglobal.so libglobal.a
          HINTS "$ENV{CHASTE_DIR}/lib" "${CHASTE_DIR}/libs"
          )

  # Search for libraries
  find_library(CHASTE_CELL_LIBRARY NAMES libcell_based.so libcell_based.a
               HINTS "${CHASTE_DIR}/lib")

  find_library(CHASTE_HEARTH_LIBRARY NAMES libheart.so libheart.a
               HINTS "${CHASTE_DIR}/lib") 
 
  # Look for Chaste in default search paths.
  # find_path(CHASTE_INCLUDE include/Version.hpp)
  
  mark_as_advanced(CHASTE_INCLUDE_DIRS)
  mark_as_advanced(CHASTE_LIBRARY_DIRS)

  set (CHASTE_LIBRARIES "")

  if(CHASTE_INCLUDE_DIRS)
    message("CHASTE FOUND")
  
    # CHASTE found
    set(CHASTE_FOUND TRUE)
    #set(CHASTE_INCLUDE_DIR ${CHASTE_INCLUDE})

    #TODO add chaste package selection for heart/cell only libs
    set(CHASTE_CELL_LIST_FILENAMES libcell_based.so libmesh.so libio.so 
        liblinalg.so libglobal.so libode.so libpde.so libcontinuum_mechanics.so)
    set(CELL_BASED_LIBRARIES "")
    message("CHASTE_CELL_LIST_FILENAMES=  ${CHASTE_CELL_LIST_FILENAMES}")    
    # Process all libraries and set _FOUND to false if any are missing
    foreach (lib_i ${CHASTE_CELL_LIST_FILENAMES})
      message("lib_i=" ${lib_i})
      # if (${lib_i})
        list  (APPEND CHASTE_LIBRARIES "${CHASTE_LIBRARY_DIRS}/${lib_i}")
        #mark_as_advanced(${i})
      #else (${i})
      #  set (${PREFIX}_FOUND FALSE)
      #endif (${lib_i})
    endforeach (lib_i)
    #endif(CHASTE_INCLUDE_DIRS)


    message( STATUS "Found CHASTE: includes in ${CHASTE_INCLUDE_DIRS} "
                    " and libraries in ${CHASTE_LIBRARY_DIRS}" )
    message("CELL_BASED_LIBRARIES= ${CELL_BASED_LIBRARIES}")
    
    else(CHASTE_INCLUDE_DIRS)
    
    # Chaste was not found.
    set(CHASTE_FOUND FALSE)  
    if(CHASTE_FIND_REQUIRED)
      message(FATAL_ERROR "CHASTE NOT FOUND! Please define environment variable CHASTE_DIR "
                          " with the location of the Chaste installation directory "
                          "  containing include/ and lib/")

    else(CHASTE_FIND_REQUIRED)
      if (NOT CHASTE_FIND_QUIETLY)
        message(STATUS "CHASTE not found")
      endif(NOT CHASTE_FIND_QUIETLY)
    endif(CHASTE_FIND_REQUIRED)
  endif(CHASTE_INCLUDE_DIRS)

#set(CHASTE_LIBRARIES ${CELL_BASED_LIBRARIES})
################################################################################
# Add dependencies

# Check for VTK
#if (GPUCANCER_ENABLE_VTK)
  find_package(VTK HINTS ${VTK_DIR} $ENV{VTK_DIR} NO_MODULE)
  set(VTK_VERSION "${VTK_MAJOR_VERSION}.${VTK_MINOR_VERSION}")
  if (VTK_FOUND)
    if ("${VTK_VERSION}" VERSION_LESS "5.8")
      set(VTK_FOUND FALSE)
      message(WARNING "Unable to find VTK (>= 5.8)")
    else()
      message(STATUS "Found VTK: ${VTK_DIR} (found version \"${VTK_VERSION}\")")
      message(STATUS "in directories ${VTK_LIBRARY_DIRS}")
      list(APPEND CHASTE_DEP_LIBS ${VTK_LIBRARIES})
      list(APPEND CHASTE_DEP_INCLUDES  ${VTK_INCLUDES})
      list(APPEND CHASTE_DEP_LIBS_DIRS  ${VTK_LIBRARY_DIRS})
    endif()
  endif()
#endif()
#-------------------------------------------------------------------------------
# Find Boost Libraries

set(CUCHASTE_BOOST_COMPONENTS   system filesystem serialization  REQUIRED)
find_package(Boost 1.54 COMPONENTS ${CUCHASTE_BOOST_COMPONENTS} REQUIRED)

list(APPEND CHASTE_DEP_LIBS      ${Boost_LIBRARIES})
list(APPEND CHASTE_DEP_INCLUDES  ${Boost_INCLUDE_DIR})

#list(APPEND CUCHASTE_TARGET_LINK_LIBRARIES_DIRS ${Boost_LIBRARY_DIRS})
message(" cuchaste libs after boost=" ${CUCHASTE_TARGET_LINK_LIBRARIES})

#-------------------------------------------------------------------------------
find_package(HDF5 REQUIRED)

list(APPEND CHASTE_DEP_LIBS ${HDF5_LIBRARIES})
list(APPEND CHASTE_DEP_INCLUDES ${HDF5_INCLUDES})

#message("HDF5_LIBRARIES=" ${HDF5_LIBRARIES})
#list(APPEND CUCHASTE_TARGET_LINK_LIBRARIES  ${HDF5_LIBRARIES})

#------------------------------------------------------------------------------
# Check for UMFPACK

  find_package(AMD QUIET)
  find_package(BLAS QUIET)
  find_package(UMFPACK QUIET)
  
  list(APPEND CHASTE_DEP_LIBS  ${AMD_LIBRARIES}
      ${BLAS_LIBRARIES} ${UMFPACK_LIBRARIES})
  list(APPEND CHASTE_DEP_INCLUDES  ${AMD_INCLUDES}
      ${BLAS_INCLUDES} ${UMFPACK_INCLUDES})  
  
#-------------------------------------------------------------------------------
# PETSc

find_package(PETSc REQUIRED)

list(APPEND CHASTE_DEP_INCLUDES ${PETSC_INCLUDE_DIRS})
list(APPEND CHASTE_DEP_LIBS ${PETSC_LIBRARIES})

#-------------------------------------------------------------------------------
find_package(LAPACK REQUIRED) 

list(APPEND CHASTE_DEP_LIBS ${LAPACK_LIBRARIES})
list(APPEND CHASTE_DEP_INCLUDES ${LAPACK_INCLUDES})

#-------------------------------------------------------------------------------
